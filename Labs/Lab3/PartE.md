[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)


### Part E - Digital Debouncer. ###
**Synopsis:** In this section we will be adding a digital switch debouncer.

### Part E1 - Digital Debounce Module ###
**Objective:** We would like to have a digital debounce stand along class that
we can call from our server SSE section that is constantly sampling the switch
input.  Create a new file called ```debouncer.py```.  In this file we will
implement a digital shift register to debounce a stream of incoming raw switch
states.  We'll only make this a 4 bit shift register since the switch sampling
rate will be 100ms.

```python
SHIFT_MASK = 15
class Debouncer(object):
    """Shift Register Debouncer"""

    def __init__(self):
        self.switch_shift_register = 0

    # perform AND logic debouncer
    def debounce(self, switch_in):
        self.switch_shift_register = (self.switch_shift_register << 1) | switch_in
        return int((self.switch_shift_register & SHIFT_MASK) == SHIFT_MASK)
```

### Part E2 - Digital Debounce Unit Test ###
**Objective:** Let's create a unit test before we just embedded our new Module
into our web server.  It will be called ```debouncer_test.py```.  In this file
we will sample the raw switch state and pass it to the debouncer.  We'll use the
LEDs to show both the state of the raw and debounced output.
```python
#!/usr/bin/python
import time
from debouncer import Debouncer
from gpio import PiGpio

# create an instance of the pi gpio driver.
pi_gpio= PiGpio()
# create an instance of the switch debouncer
db = Debouncer()
#
print('Debounce my Input Switch (Ctrl-C to stop)...')
while True:
    switch_raw = pi_gpio.read_switch()
    switch_debounced = db.debounce(switch_raw)
    pi_gpio.set_led(1,(switch_raw == 1))
    pi_gpio.set_led(2,(switch_debounced == 1))

    print('SW RAW: {0} SW DEBOUNCED: {1}'.format(switch_raw , switch_debounced))
    time.sleep(0.2)
```

OK, now that our debouncer is working, we can implement it in the web server.

[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)
