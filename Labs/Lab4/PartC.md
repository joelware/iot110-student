[LAB4 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/setup.md)

## Setting up BMP280 Unit Test

## Objectives
* Create unit test for checking out Methods of BMP280
* Create unit test for abstract methods of multiple BMP280 sensors.

### Step C1 – imports
```python
import pprint
from bmp280 import PiBMP280

```


### Step C2 – Array of BMP280 Sensor Parameters
```python
# create an array of my pi bmp280 sensor dictionaries
sensor = []
sensor.append({'name' : 'bmp280', 'addr' : 0x76, 'chip' : PiBMP280(0x76) , 'data' : {}})

```


### Step C3 – Get Sensor ID Populated
```python
# Read the Sensor ID for 0x76 -> values into the ['data'] dictionary
(chip_id, chip_version) = sensor[0]['chip'].readBMP280ID()
sensor[0]['data']['chip_id'] = chip_id
sensor[0]['data']['chip_version'] = chip_version
```

### Step C1 – Get the Sensor Data and print the Object
```python
print "  ============================== SENSOR   =============================="
print "  Chip ADDR :", hex(sensor[0]['addr'])
print "    Chip ID :", sensor[0]['data']['chip_id']
print "    Version :", sensor[0]['data']['chip_version']

# Read the Sensor Temp/Pressure values into the ['data'] dictionary
(temperature, pressure) = sensor[0]['chip'].readBMP280All()
sensor[0]['data']['temperature'] = { 'reading': temperature, 'units' : 'C' }
sensor[0]['data']['pressure'] = { 'reading': pressure, 'units' : 'hPa' }

print "Temperature :", sensor[0]['data']['temperature']['reading'], "C"
print "   Pressure :", sensor[0]['data']['pressure']['reading'] , "hPa"

pprint.pprint(sensor[0])
print "  ======================================================================"

```

[PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartD.md) Test Everything

[LAB4 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/setup.md)
